<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartBrand extends Model
{
    function parts(){
        return $this->hasMany(Part::class);
    }
}
