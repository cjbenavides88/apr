<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $casts = [
        'info' => 'array',
        'data' => 'array',
    ];


    function brand(){
        return $this->belongsTo(VehicleBrand::class, 'vehicle_brand_id');
    }

    function parts(){
        return $this->belongsToMany(Part::class)->select('data','id','info','name','type','part_brand_id')->with('brand');
    }
}
