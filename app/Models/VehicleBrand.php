<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleBrand extends Model
{
    protected $casts = [
        'info' => 'array',
        'data' => 'array',
    ];

    function vehicles(){
        return $this->hasMany(Vehicle::class)->select('id','year','name', 'info','data','vehicle_brand_id')
            ->with('parts')
            ->orderBy('year','desc');
    }

//    function vehiclesWithParts(){
//        return $this->hasMany(Vehicle::class)->select('id','year','name', 'info','data','vehicle_brand_id')->with('parts');
//    }
}
