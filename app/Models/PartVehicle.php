<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartVehicle extends Model
{
    protected $table ='part_vehicle';
}
