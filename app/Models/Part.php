<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    protected $casts = [
        'info' => 'array',
        'data' => 'array',
    ];

    function brand(){
        return $this->belongsTo(PartBrand::class,'part_brand_id');
    }

    function vehicles(){
        return $this->belongsToMany(Vehicle::class);
    }
}
