<?php

namespace App\Http\Requests\Quote;

use App\Mail\QuoteMail;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Mail;

class QuoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'email'     => 'required|email',
            'comments'  => 'required',
            'items'     => 'required|array',
        ];
    }

    public function quote(){
        //$this->input('email')
        Mail::to('cjbenavides88@gmail.con')->send(new QuoteMail($this->all()));
        return $this->all();
    }
}
