<?php

namespace App\Http\Requests\SocialMedia;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Cache;
use App\Helpers\Instagram\Instagram;

class InstagramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function getPosts(int $count = 1){
        $posts = null;
        $key = 'instagram-feed';
        if(Cache::has($key)){
            $posts = json_decode(Cache::get('instagram-feed'));
        }else{
            $instagram = new Instagram(env('INSTAGRAM_ACCESS_KEY'));
            
            $media = $instagram->media(['count' => 10]);
            $posts = [];
            foreach ($media as $item) {
                $comments = self::getComments($item->id);
                $item->post_comments = $comments;
                array_push($posts,$item);
            }
           // Cache::put($key, json_encode($posts), now()->addMinutes(15));
            Cache::put($key, json_encode($posts), now()->addMinutes(360));
            $posts = json_decode(Cache::get('instagram-feed'));
        }
        return $posts;
    }


//    public function getPosts(int $count = 1){
//        $posts = null;
//        $key = 'instagram-feed';
//        if(Cache::has($key)){
//            $posts = json_decode(Cache::get('instagram-feed'));
//        }else{
//            $instagram = new Instagram(env('INSTAGRAM_ACCESS_KEY'));
//            Cache::put($key, json_encode($instagram->media(['count' => 10])), now()->addMinutes(15));
//            $posts = json_decode(Cache::get('instagram-feed'));
//        }
//        return $posts;
//    }
    
    public function getComments($mediaID){

        $instagram = new Instagram(env('INSTAGRAM_ACCESS_KEY'));
        return $instagram->comments((string)$mediaID);
    }
}
