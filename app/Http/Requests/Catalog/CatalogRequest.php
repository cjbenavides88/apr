<?php

namespace App\Http\Requests\Catalog;

use App\Models\VehicleBrand;
use Illuminate\Foundation\Http\FormRequest;

class CatalogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function getCatalog(){
        $brands = VehicleBrand::with('vehicles')
            ->select('id','name','description','data')
            ->get()
            ->sortByDesc('name');
        return $brands;
    }
}
