<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Requests\Catalog\CatalogRequest;
use App\Models\VehicleBrand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class CatalogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @param  \App\Http\Controllers\Catalog\
     * @return \Illuminate\Support\Collection
     */
    public function vehiclesBrands(CatalogRequest $request)
    {
        return $request->getCatalog();
    }
}
