<?php

namespace App\Http\Controllers\SocialMedia;

use App\Http\Requests\SocialMedia\InstagramRequest;
use App\Http\Controllers\Controller;


class SocialMediaController extends Controller
{
    public function instagram(InstagramRequest $request){
        return response()->json($request->getPosts(10));
    }

    public function instagramComments(InstagramRequest $request){
        return response()->json($request->getComments((string)$request['id']));
    }
}
