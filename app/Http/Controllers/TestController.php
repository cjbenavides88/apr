<?php

namespace App\Http\Controllers;

use App\Models\Part;
use App\Models\PartBrand;
use App\Models\PartVehicle;
use App\Models\Vehicle;
use App\Models\VehicleBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    function index(){
        $vehicles = Vehicle::all();

        for($x = 0; $x <= 10; $x++){
            foreach ($vehicles as $vehicle) {
                $pivot = new PartVehicle();
                $pivot->vehicle_id = $vehicle->id;

                $part = Part::inRandomOrder()->first();

                $pivot->part_id = $part->id;
                $pivot->save();
            }
        }

    }

    function testVehicle(){
        DB::connection()->enableQueryLog();
        $vehicle = Vehicle::first();

        $vehicle->brand;
        $brand = $vehicle->parts->pluck('name');


        //  dd(DB::getQueryLog());
        $data = [
            'vehicle' => $vehicle,
            'data'  => $brand,
            'queries' => DB::getQueryLog()
        ];

        return $data;
    }

    function testPart(){
        DB::connection()->enableQueryLog();
        $part = Part::where('id', 7)->first();

        $part->brand;
        $brand = $part->vehicles->pluck('name');


        //  dd(DB::getQueryLog());
        $data = [
            'Part' => $part->name,
            'data'  => $brand,
            'queries' => DB::getQueryLog()
        ];

        return $data;
    }

    function vehicleBrands(){
        $brands = VehicleBrand::with('vehicles')->select('id','name','description','data')->get()->sortByDesc('name');
        dd($brands);
        return $brands;
    }


    function mail(Request $request){
       // session()->put('quote',$request->all());
        $data = session()->get('quote');
        return view('emails.quote.user')->with(['data' => $data]);

    }
}
