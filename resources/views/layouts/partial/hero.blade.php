<section class="hero is-primary is-large has-bg-img">
    <!-- Hero head: will stick at the top -->
    <div class="hero-head">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-brand">
                    <a class="navbar-item">
                        <img src="https://www.goapr.com/includes/img/logos/apr_white_200.png" alt="Logo">
                    </a>
                    <span class="navbar-burger burger" data-target="navbarMenuHeroA">
            <span></span>
            <span></span>
            <span></span>
          </span>
                </div>
                <div id="navbarMenuHeroA" class="navbar-menu">
                    <div class="navbar-end">
                        <a class="navbar-item is-active">
                            Home
                        </a>
                        <a class="navbar-item">
                            Examples
                        </a>
                        <a class="navbar-item">
                            Documentation
                        </a>
                        <span class="navbar-item">
              <a class="button is-primary is-inverted">
                <span class="icon">
                  <i class="fab fa-github"></i>
                </span>
                <span>Download</span>
              </a>
            </span>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <!-- Hero content: will be in the middle -->
    <div class="hero-body">
        <div class="container has-text-centered">
            {{--<h1 class="title">--}}
                {{--APR--}}
            {{--</h1>--}}
            {{--<h2 class="subtitle">--}}
                {{--Enhancing the Driving Experience--}}
            {{--</h2>--}}
        </div>
    </div>

    <!-- Hero footer: will stick at the bottom -->
    <div class="hero-foot">
        <nav class="tabs is-boxed is-centered">
            <div class="container">
                <ul>
                    <router-link tag='li' to="/" exact>
                        <a class="hvr-icon-pulse-grow">
                            <span class="icon is-small"><i class="fas fa-home hvr-icon"></i></span>
                            <span>Home</span>
                        </a>
                    </router-link>
                    <router-link tag='li' to="/products">
                        <a class="hvr-icon-pulse-grow">
                            <span class="icon is-small"> <i class="fas fa-car hvr-icon"></i></span>
                            <span>Products</span>
                        </a>
                    </router-link>
                    <router-link tag='li' to="/dealers">
                        <a class="hvr-icon-pulse-grow">
                            <span class="icon is-small"><i class="fas fa-store-alt hvr-icon"></i></span>
                            <span>Dealers</span>
                        </a>
                    </router-link>
                    <router-link tag='li' to="/contact">
                        <a class="hvr-icon-pulse-grow">
                            <span class="icon is-small"><i class="far fa-envelope hvr-icon"></i></span>
                            <span>Contact</span>
                        </a>
                    </router-link>
                </ul>
            </div>
        </nav>
    </div>
</section>