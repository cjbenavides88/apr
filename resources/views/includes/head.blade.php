<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- APP CSS -->
<link rel="stylesheet" href="{{asset(mix('css/app.css'))}}">
<link rel="shortcut icon" href="img/favicon.png"  />

<title>APR - Prototype</title>