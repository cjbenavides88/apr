@extends('layouts.layout')

@section('content')

    <section id="app">
        @include('layouts.partial.hero')
        <section class="section">
            <transition name="router" mode="out-in">
                <router-view class="container view-container"></router-view>
            </transition>
        </section>
    </section>
@endsection