export default [
    {
        path: '/' ,
        component: require('./views/HomeView').default
    },
    {
        path: '/products' ,
        component: require('./views/ProductsView').default
    },
    {
        path: '/dealers' ,
        component: require('./views/DealersView').default
    },
    {
        path: '/contact' ,
        component: require('./views/ContactView').default
    },
];