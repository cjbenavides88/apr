<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::any('/quote', 'TestController@mail');

//CATALOG
Route::group(['prefix' => 'catalog','namespace' => 'Catalog'], function (){
    Route::get('get-vehicle-brands','CatalogController@vehiclesBrands');
    Route::get('get-vehicles','CatalogController@vehicles');
    Route::get('get-parts-brands','CatalogController@partsBrands');
});

//SOCIAL MEDIA
Route::group(['prefix' => 'social','namespace' => 'SocialMedia'], function (){
    Route::get('instagram','SocialMediaController@instagram');
    Route::get('instagram/comments','SocialMediaController@instagramComments');
});


Route::group(['namespace' => 'Quote'], function (){
 //   Route::resource('quote','QuoteController')->only(['store']);
});

