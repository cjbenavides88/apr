<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\VehicleBrand::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Fakecar($faker));
    return [
        'name'          => $faker->vehicleBrand,
        'description'   => $faker->sentence(9,true),
        'data'          =>  json_encode([
            'image' => $faker->imageUrl(1280, 960, 'transport', true)]
        ),
        'created_at'    => now(),
        'updated_at'    => now(),
    ];
});
