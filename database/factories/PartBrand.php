<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\PartBrand::class, function (Faker $faker) {

    return [
        'name'        => $faker->word,
        'description' => $faker->sentence(15,true),
        'data'  =>  json_encode(['image' => $faker->imageUrl(1280, 960, 'transport', true)]),
        'created_at' => now(),
        'updated_at' => now(),

    ];
});
