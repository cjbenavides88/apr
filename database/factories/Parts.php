<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Part::class, function (Faker $faker) {
    return [
        'name'          =>   $faker->word,
        'data'      =>  json_encode([
            'image' => $faker->imageUrl(1280, 960, 'transport', true),
        ]),
        'info'      =>  json_encode([
            'description' => $faker->paragraph,
        ]),
        'type' => 'Tool',
        'created_at' => now(),
        'updated_at' => now(),
        'part_brand_id' => function() {
                $brand = \App\Models\PartBrand::inRandomOrder()->first();
                return $brand->id;
        }
    ];
});
