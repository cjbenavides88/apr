<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Vehicle::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Fakecar($faker));
    $v = $faker->vehicleArray();
    return [
        'year'          => $faker->biasedNumberBetween(1998,2019, 'sqrt'),
        'name'          =>   $v['model'],
        'data'          =>  ([
                'image' => $faker->imageUrl(1280, 960, 'transport', true)]
        ),
        'info'      =>  ([
            'registration_no' => $faker->vehicleRegistration,
            'type' => $faker->vehicleType,
            'fuel' => $faker->vehicleFuelType,
            'model' => $v['model'],
            'brand' => $v['brand'],
        ]),
        'created_at' => now(),
        'updated_at' => now(),
        'vehicle_brand_id' => function() {
            $brand = \App\Models\VehicleBrand::inRandomOrder()->first();
            return $brand->id;
        }
    ];


});
