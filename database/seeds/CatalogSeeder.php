<?php

use App\Models\VehicleBrand;
use App\Models\PartBrand;
use App\Models\Part;
use App\Models\Vehicle;
use Illuminate\Database\Seeder;

class CatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        //Vehicle Brands

//        $vehicle_brands = [
//            [
//                'name'        => 'BMW',
//                'description' => 'BMW es un fabricante alemán de automóviles de gama alta y motocicletas, cuya sede se encuentra en Múnich.',
//                'data'  =>  json_encode(['image' => 'https://bulma.io/images/placeholders/1280x960.png' ]),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//
//            ],
//            [
//                'name'        => 'Nissan',
//                'description' => 'Nissan se remonta a Kawaishinsha Co., una fábrica de automóviles fundada por Masujiro Hashimoto en el distrito de Azabu-Hiroo, Tokio en 1911.',
//                'data'  =>  json_encode(['image' => 'https://bulma.io/images/placeholders/1280x960.png' ]),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//
//            ],
//            [
//                'name'        => 'Audi',
//                'description' => 'Audi es una empresa alemana fabricante de vehículos de alta gama, lujo y de competición con presencia global.',
//                'data'  =>  json_encode(['image' => 'https://bulma.io/images/placeholders/1280x960.png' ]),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//
//            ],
//            [
//                'name'        => 'Infinity',
//                'description' => 'Infiniti es una marca de automóviles de lujo creada por Nissan. . ',
//                'data'  =>  json_encode(['image' => 'https://bulma.io/images/placeholders/1280x960.png' ]),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//            ]
//        ];
//        DB::table('vehicle_brands')->insert($vehicle_brands);
//
//        //Parts Brands``
//        $part_brands = [
//            [
//                'name'        => 'Eventury',
//                'description' => 'Donec vestibulum neque at laoreet molestie. Duis elementum dui eget nibh ornare maximus.',
//                'data'  =>  json_encode(['image' => 'https://bulma.io/images/placeholders/1280x960.png' ]),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//
//            ],
//            [
//                'name'        => 'APR',
//                'description' => 'Donec vestibulum neque at laoreet molestie. Duis elementum dui eget nibh ornare maximus.',
//                'data'  =>  json_encode(['image' => 'https://bulma.io/images/placeholders/1280x960.png' ]),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//
//            ],
//            [
//                'name'        => 'Racing Line',
//                'description' => 'Donec vestibulum neque at laoreet molestie. Duis elementum dui eget nibh ornare maximus.',
//                'data'  =>  json_encode(['image' => 'https://bulma.io/images/placeholders/1280x960.png' ]),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//
//            ],
//            [
//                'name'        => 'Performance Supply',
//                'description' => 'Donec vestibulum neque at laoreet molestie. Duis elementum dui eget nibh ornare maximus.',
//                'data'  =>  json_encode(['image' => 'https://bulma.io/images/placeholders/1280x960.png' ]),
//                'created_at' => date('Y-m-d H:i:s'),
//                'updated_at' => date('Y-m-d H:i:s'),
//            ]
//        ];
//        DB::table('part_brands')->insert($vehicle_brands);

//        factory(PartBrand::class,4)->create();
//        factory(Part::class,100)->create();
//
//        factory(VehicleBrand::class,12)->create();
        factory(Vehicle::class,50)->create();
    }
}
